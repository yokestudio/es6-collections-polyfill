/**
 * Polyfills for ES6 Set and Map
 *
 * - Number.isNaN() will also be polyfilled because this script needs proper NaN detection.
 * - In Chrome 43 / Opera 30, Set constructor doesn't accept NodeList/HTMLCollection as arguments.
 *   This is polyfilled as well.
 *
 * Note: WeakSets and WeakMaps are not implemented.
 */

// Patch Number.isNaN (from ES6 number polyfill)
Number.isNaN = Number.isNaN || function(v) {return typeof v === 'number' && v !== v;};

// Patch browsers that don't implement NodeList and HTMLCollection as iterables
(function() {
	// Skip browsers that have not yet implemented Symbols
	if (typeof Symbol === 'undefined' || typeof Symbol.iterator !== 'symbol') {
		return;
	}
	
	var i, len;      // loop vars
	var friends = [  // list of iterable objects to patch
		//DOMSettableTokenList,
		//DOMTokenList,
		HTMLCollection,
		NodeList
	];
	for (i=0,len=friends.length; i<len; i++) {
		if (typeof friends[i].prototype[Symbol.iterator] === 'function') {
			continue;
		}
		friends[i].prototype[Symbol.iterator] = Array.prototype[Symbol.iterator];
	}
})();

(function(global) {
	var needSetsPolyfill = (function() {
		try {
			var s = new Set([1,2,3]);
			if (s.size !== 3 || !s.has(2)) {
				//console.log('Set constructor cannot accept arrays - we need set polyfill.');
				return true;
			}
		}
		catch (e) {
			//console.log('Exception caught - we need set polyfill.');
			return true;
		}
	
		return false;
	})();
	var needMapsPolyfill = (function() {
		try {
			var m = new Map([[1,2],[3,4]]);
			if (m.size !== 2 || !m.has(1)) {
				//console.log('Map constructor cannot accept arrays - we need set polyfill.');
				return true;
			}
		}
		catch (e) {
			//console.log('Exception caught - we need map polyfill.');
			return true;
		}
		
		return false;
	})();
	
	if (needSetsPolyfill) {
		global['Set'] = createCollection({
			has: sharedHas,
			add: setAdd,
			'delete': sharedDelete,
			clear: sharedClear,
			keys: sharedKeys,
			values: sharedKeys, // Note: same as keys
			entries: setEntries,
			forEach: sharedForEach
		});
		//console.log('Sets polyfilled');
	}
	if (needMapsPolyfill) {
		global['Map'] = createCollection({
			has: sharedHas,
			get: mapGet,
			set: mapSet,
			'delete': sharedDelete,
			clear: sharedClear,
			keys: sharedKeys,
			values: sharedValues,
			entries: mapEntries,
			forEach: sharedForEach
		});
		//console.log('Maps polyfilled');
	}
	
	// Checks if an object can be iterated using forEach
	function supportsForEach(a) {
		if (typeof a !== 'object') {
			return false;
		}
		if (typeof a.forEach === 'function') {
			return true;
		}
		return false;
	}
	
	function createCollection(proto) {
		/**
		 * @constructor
		 */
		function Collection(arg){
			if (!this || this.constructor !== Collection) {
				return new Collection(arg);
			}
			
			this._keys = []; // only really used by Maps
			this._values = [];
			this._itp = []; // iteration pointers
			Object.defineProperty(proto, 'size', {
				get: sharedSize
			});
			
			// Add elems if constructor is called with an argument
			var collection = this;
			if (arg === null || typeof arg === 'undefined') {
				//ignore null values
			} // null / undefined args
			else if (typeof arg === 'string') {
				// strings are iterable
				if (this.add) {
					for (var i=0,len=arg.length; i<len; i++) {
						collection.add(arg.charAt(i));
					}
				} // Set
				else if (this.set) {
					throw TypeError('iterable for Map should have array-like objects');
				} // Map
			} // argument is string
			else if (typeof arg === 'object' && arg !== null) {
				if (arg instanceof Set) {
					if (this.add) {
						arg.forEach(function(v) {
							collection.add(v);
						});
					} // adding Set to Set
					else if (this.set) {
						throw TypeError('iterable for Map should have array-like objects');
					} // adding Set to Map
				} // Set
				else if (arg instanceof Map) {
					if (this.add) {
						arg.forEach(function(v, k) {
							collection.add([k, v]);
						});
					} // adding Map to Set
					else if (this.set) {
						arg.forEach(function(v, k) {
							collection.set(k, v);
						});
					} // adding Map to Map
				} // Map
				else {
					if (this.add) {
						if (typeof arg.length !== 'number') {
							throw TypeError('iterable for Set should have array-like objects');
						}
						var arr = [].slice.call(arg); // ensure arg is converted to an array (assume Array-like)
						arr.forEach(function(v) {
							collection.add(v);
						});
					} // adding object to Set
					else {
						if (typeof arg.length !== 'number') {
							throw TypeError('iterable for Map should have array-like objects');
						}
						var arr = [].slice.call(arg); // ensure arg is converted to an array (assume Array-like)
						arr.forEach(function(v) {
							collection.set(v[0], v[1]);
						});
					} // adding object to Map
				} // Other object
			} // argument is Object
			else {
				throw TypeError(arg + 	' is not iterable');
			}
		} // Collection constructor
		
		// Set constructor method
		proto.constructor = Collection;
		Collection.prototype = proto;
		return Collection;
	} //createCollection()

	function getIndexOf(list, v) {
		if (Number.isNaN(v)) {
			for (var i=0,len=list.length; i<len; i++) {
				if (Number.isNaN(list[i])) {
					return i;
				}
			}
		}
		else {
			for (var i=0,len=list.length; i<len; i++) {
				if (list[i] === v) {
					return i;
				}
			}
		}
		return -1;
	}
	
	function setAdd(v) {
		if (!this.has(v)) {
			this._keys.push(v);
		}
		return this;
	}
	
	function mapGet(k) {
		var item_idx = getIndexOf(this._keys, k);
		
		if (item_idx > -1) {
			return this._values[item_idx];
		}
		else {
			return undefined;
		}
	}
	function mapSet(k, v) {
		var item_idx = getIndexOf(this._keys, k);
		
		if (item_idx > -1) {
			this._values[item_idx] = v;
		}
		else {
			var len = this._keys.push(k);
			this._values[len - 1] = v;
		}
		return this;
	}
	
	function sharedHas(k) {
		return (getIndexOf(this._keys, k) > -1);
	}
	
	function sharedDelete(k) {		
		// Find item in list
		var item_idx = getIndexOf(this._keys, k);
		
		if (item_idx > -1) {
			this._keys.splice(item_idx, 1);
			this._values.splice(item_idx, 1);
			
			// update iteration pointers
			this._itp.forEach(function(p) {
				if (item_idx < p[0]) p[0]--;
			});
			
			return true;
		} // item found
		
		return false;
	} //sharedDelete()
	
	function sharedClear() {
		this._keys.length = 0;
		this._values.length = 0;
	}
	
	function sharedSize() {
		return this._keys.length;
	}

	function sharedKeys() {
		return sharedIterator(this._itp, this._keys, undefined);
	}
	
	function sharedValues() {
		return sharedIterator(this._itp, this._values, undefined);
	}
	
	function mapEntries() {
		return sharedIterator(this._itp, this._keys, this._values);
	}
	
	function setEntries() {
		return sharedIterator(this._itp, this._keys, this._keys);
	}
	
	function sharedForEach(callback, context) {
		var iterator = this.entries();
		var obj = iterator.next();
				
		while (!obj.done) {
			callback.call(context, obj.value[1], obj.value[0], this);
			obj = iterator.next();
		}
	} //sharedForEach()
	
	// @private
	function sharedIterator(itp, array, array2) {
		// Add new iterator pointer
		var ptr = [0]; // ensure pointer is stored as an object internally
		itp.push(ptr);
		
		var done = false;
		function next() {
			var val;
			var i = ptr[0];
			if (!done && i < array.length) {
				if (typeof array2 !== 'undefined') {
					val = [array[i], array2[i]];
				} // two arrays are passed as args: return as array of [key, value]
				else {
					val = array[i];
				}
				ptr[0]++;
			}
			else {
				done = true;
				itp.splice(itp.indexOf(ptr), 1); // remove pointer once it's no longer needed
			}
			return {
				done: done,
				value: val
			};
		}
		
		return {next: next};
	} //sharedIterator()
})(this);
