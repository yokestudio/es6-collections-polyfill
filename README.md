# ES6 Collections Polyfill #

A JavaScript polyfill for ES6 Sets and Maps.

### Features ###
* Simulated iterator behaviour for Sets and Maps.
* Similar behaviour across all browsers (Tested: IE 10+, Chrome 43, Firefox 39, Opera 30).

### Set Up ###

1. Download the latest release (as of 9 Jul 2015, v1.0.0).
2. Include in your HTML (preferably just before `</body>`):

    `<script src="es6-collections-polyfill-1.0.0.min.js"></script>`

### API ###

Refer to [Set API](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set),
[Map API](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map).

### FAQ ###

1. Why don't you simply use an existing polyfill?
> None of them work fully across all browsers, especially with regards to Set/Map constructors supporting iterables (including strings). The best one we found ([Paul Miller's ES6 Shim](https://github.com/paulmillr/es6-shim)) doesn't pass all our tests, and it is **huge** since it does more than just Sets and Maps.

1. Does this polyfill have any bugs/limitations?
> This polyfill does NOT implement WeakMap and WeakSet; the benefit of using them comes only in a true ES6 setting, with proper garbage collection on objects without other references. Since we cannot polyfill the garbage collection behaviour easily, polyfilling WeakSet and WeakMap might actually end up being counter-productive.
>
> Although this polyfill enables you to simulate iterator behaviour, it is NOT a polyfill for real iterators and `for...of`, or `Symbol`.
>
> If you find any bugs/limitations, please let us know.

### References ###
[WebReflection](https://github.com/WebReflection/es6-collections)
[Paul Miller's ES6 Shim](https://github.com/paulmillr/es6-shim)

### Contact ###

* Email us at <yokestudio@hotmail.com>.